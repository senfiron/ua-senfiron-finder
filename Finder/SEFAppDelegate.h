//
//  SEFAppDelegate.h
//  Finder
//
//  Created by Senfiron on 19.03.14.
//  Copyright (c) 2014 Senfiron. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SEFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
