//
//  main.m
//  Finder
//
//  Created by Senfiron on 19.03.14.
//  Copyright (c) 2014 Senfiron. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SEFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SEFAppDelegate class]));
    }
}
